<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
<?php 
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");
echo "$sheep->name <br>"; // "shaun" 
echo "$sheep->legs <br>"; // 2
echo "$sheep->cold_blooded <br>"; // false
echo "=====================<br>";
$sungokong = new Ape("kera sakti");
echo "$sungokong->name <br>"; 
echo "$sungokong->legs <br>"; 
echo $sungokong->yell() ;// "Auooo"
echo "<br>=====================<br>";
$kodok = new Frog("buduk");
echo "$kodok->name <br>"; 
echo "$kodok->legs <br>"; 
$kodok->jump() ; // "hop hop"



?>

</body>
</html>
